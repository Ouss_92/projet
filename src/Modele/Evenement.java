package Modele;

import javax.swing.*;

public class Evenement implements Comparable <Evenement>{
	private String chTitre;
	private Date chDate;
	private int chPoids;
	private String chDesc;
	private ImageIcon chPath;
	private static int chNbrEvt = 0;
	private JLabel chLab[];
	
	
	public String toString(){
		return chPath +"\n"+ chDate+"\n"+chTitre+"\n"+chDesc;
	}
	
	
	public void setDate (Date parDate){
		chDate = parDate;
		}
		
		public void setTitre (String parTitre)
		{
		chTitre= parTitre;
		}	
		
		public void setDesc(String parDesc) {
			
			chDesc = parDesc;
			
		}

		
		public void setPath(ImageIcon parPath) {
			
			chPath = parPath;
			
		}
		
		public String getTitre(){
			return chTitre;
		}
		
		public Date getDate(){
			return chDate;
		}
		
		public String getDescription() {
			
			return chDesc;
			
		}
		
		public ImageIcon getPath() {
			
			return chPath;
			
		}
		
		public Evenement(ImageIcon parPath, Date parDate, String parTitre, int parPoids, String parDesc) {

			chPath = parPath;
			chDate= parDate;
			chTitre = parTitre;
			chPoids = parPoids; 
			chDesc = parDesc;
			
			chNbrEvt++;
			
		}
		
		public int getNbrEvt(){
			return chNbrEvt;
		}
		
		public int compareTo(Evenement parEvenement){
			if (chDate.compareTo(parEvenement.chDate) != 0)
				return (chDate.compareTo(parEvenement.chDate));
			
			return (chPoids - parEvenement.chPoids);

		}
		
		
		
		
} 

