package Modele;

import javax.swing.table.*;

/**
 * 
 * @author touns
 *
 */
public class ModeleTable extends DefaultTableModel{
	
		/**
		 * 
		 * @param parDate
		 * @param parChrono
		 */
		public ModeleTable (Date parDate, Chronologie parChrono) {
			
			setColumnCount(90);
			setRowCount(4);
			parDate = new Date(13, 07, 1930);
			Date d = parDate;
			String[] entete = new String[89];
			
			
			for(int i = 0; i < entete.length; i++) {
				
				if(i%4 == 0) {
					
				entete[i] = " " + d.getAnnee() + " ";
				d = d.dateCoupeDuMonde();
				
				}
				
				else
					entete[i] = " ";
			}
			
			setColumnIdentifiers(entete);
			
		}
		
		/**
		 * @return boolean
		 */
		public boolean isCellEditable (int indiceLigne, int indiceColonne) {
			
			return false;
			
		}
		
		/**
		 * @return class
		 */
		public Class getColumnClass (int parNum) {
			
			return Evenement.class;
		}
}