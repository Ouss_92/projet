package Modele;

import java.io.File;

import java.util.* ;

import javax.swing.*; 

/**
 * 
 * @author touns
 *
 */
public class Chronologie {
	
	private static int chNbrEvt = 0;
	
	
	private TreeSet<Evenement> chTreeSet = new TreeSet <Evenement>();
		
	/**
	 * Ajoute un evenement au TreeSet d'Evenement
	 * @param parEvt
	 * @return boolean
	 */
	public boolean ajout (Evenement parEvt){
		 chTreeSet.add(parEvt);
		 return true;
	}
	
	/**
	 * Retourne le nombre d'evenements
	 * @return int
	 */
	public int getNbrEvt(){
		return chNbrEvt;
	}
	
	
	/**
	 * 
	 * @param args
	 */
	public static void main (String [] args){
		Chronologie c = new Chronologie();
		
		c.ajout(new Evenement(new ImageIcon("images"+File.separator+"cdmu.png"), new Date(16, 7, 1930),"Uruguay coupe du monde 1930",  1, "L'Uruguay remporte sa deuxi�me coupe du monde en battant le Br�sil chez elle."));
		c.ajout(new Evenement(new ImageIcon("images"+File.separator+"cdmic.png"), new Date(2, 6, 1962),"Italie-Chilie", 2, "Le match le plus violent de l'histoire de la coupe du monde." ));
		c.ajout(new Evenement(new ImageIcon("images"+File.separator+"cdmf.png"), new Date(12, 7, 1998),"France coupe du monde 1998", 3, "En 1998 l'�quipe de France remporte sa premi�re coupe du monde."));
		c.ajout(new Evenement(new ImageIcon("images"+File.separator+"cdmv.png"), new Date(9,7,2006),"Coup de boule", 4, "Zinedine Zidane ass�ne un violent coup de boule au d�fenseur italien Marco Materrazi."));
		
		System.out.println(c);
	}
	
	
}