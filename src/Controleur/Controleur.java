package Controleur;

import java.awt.event.*;



import Modele.*;

import java.io.*;
import Vue.*;
import java.util.*;

/**
 * 
 * @author touns
 *
 */
public class Controleur implements ActionListener, Serializable {
	
	//File fichier = new File("saves"+File.separator+"frise.ser");
	Chronologie chChrono;
	FenetreMere chFen;
	PanelChronologie chChronologie;
	PanelFormulaire chForm;
	PanelFrise chFrise;

	/**
	 * Controleur de la classe Chronologie
	 * @param parChrono
	 */
	public Controleur(Chronologie parChrono) {
		
		chChrono = parChrono;
	}
	
	/**
	 * Controleur de classe FenetreMere
	 * @param fenMere
	 */
	public Controleur(FenetreMere fenMere) {
		
		chFen = fenMere;
		chFen.enrEcouteur(this);
		
		
	}

	/**
	 * Controleur de la classe PanelChronologie
	 * @param panelChrono
	 */
	public Controleur(PanelChronologie panelChrono) {
		
		chChronologie = panelChrono;
		
	}
	
	/**
	 * Controleur de la classe PaanelFormulaire
	 * @param panelForm
	 */
	public Controleur(PanelFormulaire panelForm) {
		
		chForm = panelForm;
		chForm.enrEcouteur(this);
		
	}
	
	/**
	 * Conntroleur de la classe PanelFrise
	 * @param panelFrise
	 */
	public Controleur(PanelFrise panelFrise) {
		
		chFrise = panelFrise;
		chFrise.enrEcouteur(this);
	}
	
	
	/**
	 * Actions liees aux clics de l'utilisateur
	 */
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == chForm.getBouton()){
			System.out.println("OYE");
			//Evenement e2 = chForm.getEvt();
			//chChrono.ajout(e2);
		     //System.out.println(chChrono.toString());
			//LectureEcriture.ecriture(fichier,agenda);
		}
		
		else if(e.getSource() == chFen.getQuitter()) {
			
			System.exit(0);
			
		}

	}

}