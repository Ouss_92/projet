package Vue;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import javax.swing.*;

import Controleur.Controleur;
import Modele.*;

/**
 * 
 * @author touns
 *
 */
public class PanelFrise extends JPanel implements ActionListener, MouseListener{
	
	private JMenuBar chMenu = new JMenuBar();
	
	private JMenu chCreation = new JMenu("Cr�ation");
	private JMenu chAffichage = new JMenu("Affichage");
	private JMenu chQuitter = new JMenu("Quitter");
	private JMenu chAide = new JMenu("?");
	
	private JMenuItem chAjouter = new JMenuItem("Ajouter un evenement");
	
	Evenement [] evt = new Evenement [4];
	int indice = 0;
	
	
	//***************DEFINITION DU PANEL PARTIE NORD******************
		JPanel panelNord = new JPanel();
		BorderLayout borderLayout = new BorderLayout(10,5);
		
		// Definition des composants du bas du diaporama
		JPanel panelBas = new JPanel () ;
		String chIntituleBouton[]= {"<",">"};// 1-Boutons
		JButton chBouton[]= new JButton[chIntituleBouton.length];
		
		//Definition des composants du centre du diaporama
		JPanel panelCentre = new JPanel();
		CardLayout gestionnaireCarte = new CardLayout (5,5);
		String chIntitulEtiquette [] = new String[evt.length];
		JLabel[] chEtiquette = new JLabel[chIntitulEtiquette.length];
		
		JLabel actuelIntitule = new JLabel(chIntitulEtiquette[0]);
		JLabel main_titre = new JLabel("La coupe du monde depuis 1930", JLabel.CENTER);
		
		String titre = new String();
		String date = new String();
		String desc = new String();
		
		
		//***************DEFINITION DU PANEL PARTIE SUD******************
		JPanel panelSud = new JPanel();
		GridLayout layoutSud = new GridLayout(60, 60, 4, 2);
		
		Chronologie chChrono;
		JTable chTable;
		Date d = new Date();
		ModeleTable modele;
		PanelFormulaire paf = new PanelFormulaire(chChrono);
		
		//Element qui compose ce panel
		
		/*private String[] chTitreJours = {"France coupe du monde 1998", "Uruguay coupe du monde 1930", "Italie-Chilie", "Coup de boule"};
	    private JLabel[] chEtiquettes = new JLabel[chTitreJours.length];*/
	
	
		int chIndex = 0;
		int actuel = 0;
		int chRow = 0;
		int chCol = 0;
		
	
		/**
		 * Constructeur
		 * @param parChrono
		 * @param paff
		 */
		public PanelFrise(Chronologie parChrono, PanelFormulaire paff) {

		setLayout(new BorderLayout (10,5)) ;
		
		paf = paff;

		Chronologie c = new Chronologie();

		evt[0] = new Evenement(new ImageIcon("images"+File.separator+"cdmu - Copie.png"), new Date(16, 7, 1930),"Uruguay coupe du monde 1930",  1, "L'Uruguay remporte sa deuxi�me coupe du monde en battant le Br�sil chez elle.");
		evt[1] = new Evenement(new ImageIcon("images"+File.separator+"cdmic - Copie.png"), new Date(2, 6, 1962),"Italie-Chilie", 2, "Le match le plus violent de l'histoire de la coupe du monde." );
		evt[2] = new Evenement(new ImageIcon("images"+File.separator+"cdmf - Copie.png"), new Date(12, 7, 1998),"France coupe du monde 1998", 3, "En 1998 l'�quipe de France remporte sa premi�re coupe du monde.");
		evt[3] = new Evenement(new ImageIcon("images"+File.separator+"cdmv - Copie.png"), new Date(9,7,2006),"Coup de boule", 4, "Zinedine Zidane ass�ne un violent coup de boule au d�fenseur italien Marco Materrazi.");

		panelNord.setLayout(new BorderLayout(10, 10));

		panelCentre.setLayout (gestionnaireCarte);
		
		Font font = new Font("Arial", Font.BOLD, 30);
		main_titre.setFont(font);

		panelNord.add(main_titre, BorderLayout.NORTH);

		for (int i = 0; i < evt[0].getNbrEvt(); i++) {
			
			

			chIntitulEtiquette[i] = evt[i].toString();
			
			titre = evt[i].getTitre();
			date = evt[i].getDate().toString();
			desc = evt[i].getDescription();
			
			String html = new String("<html>" + "<body>" +
									 "<h4>"+date+"</h4>" +
									 "</h2>"+titre+"</h2>"+"<br>" +
									 "<br>"+"<p>"+desc+"</p>" +
									 "</body>" + "</html>" );
			
			chEtiquette[i] = new JLabel(html, evt[i].getPath(), JLabel.CENTER);

			panelCentre.add (chEtiquette[i], chIntitulEtiquette[i]);
			
			}

	
		for (int i = 0; i < chIntituleBouton.length; i++) {
			
			chBouton[i]=new JButton(chIntituleBouton[i]);
			chBouton[i].addActionListener(this);
			
			panelBas.add(chBouton[i]);
	
		}


		panelNord.add (panelCentre, BorderLayout.CENTER); //Ajoute la zone (Contenu,Position)
		panelNord.add (panelBas, BorderLayout.SOUTH);
		
		
		setLayout(new GridLayout(0, 1, 0, -5));
		
		//chChrono = parChrono;
		modele = new ModeleTable(d, chChrono);
		
		
		chTable=new JTable(modele);
		chTable.setRowHeight(85);
		
		
		
		JScrollPane scrollPane = new JScrollPane(chTable, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		scrollPane.setPreferredSize(new Dimension(1500, 380));
		
		chTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		
		chTable.getColumnModel().getColumn(0).setCellRenderer(new CelluleRenderer());
		chTable.getColumnModel().getColumn(32).setCellRenderer(new CelluleRenderer());
		chTable.getColumnModel().getColumn(68).setCellRenderer(new CelluleRenderer());
		chTable.getColumnModel().getColumn(76).setCellRenderer(new CelluleRenderer());
		//chTable.setDefaultRenderer(Icon.class, new CelluleRenderer());
		
		panelSud.add(scrollPane);

		chTable.addMouseListener(mouseListenerEvents);
		
		
		add(panelNord);
		add(panelSud);
		setBackground(new Color(0, 0, 0));

		
	}	
	
	/**
	 * Le diaporama se met � jour en fonction du choix de l'utilisateur(aller de l'avant ou inverse)
	 * @param parEvt
	 */
	public void actionPerformed(ActionEvent parEvt){
		
		if (parEvt.getSource() == chBouton[0])
			{
				gestionnaireCarte.previous (panelCentre);
				chIndex --;
				
				if (chIndex < 0)
					chIndex = chIntitulEtiquette.length - 1;
				
			actuelIntitule.setText(chIntitulEtiquette[chIndex]);
		}
	
		else if (parEvt.getSource() == chBouton[1]) {
			
			gestionnaireCarte.next(panelCentre);
			chIndex ++;
			if (chIndex >= chIntitulEtiquette.length )
				chIndex = 0;
			actuelIntitule.setText(chIntitulEtiquette[chIndex]);
			
			
		}
		
		else if(parEvt.getSource() == paf.getBouton()) {
			
			System.out.println("YOOOO");
			
		}
	
		
	}
	
	
	/**
	 * Controleur gere l'action
	 * @param parCtrl
	 */
	public void enrEcouteur(Controleur parCtrl) {

		for(int i = 0; i < chBouton.length; i++) {
			
			chBouton[i].addActionListener(parCtrl);
			
		}
		
	}
	
	MouseListener mouseListenerEvents = new MouseListener() {

		/**
		 * Lorsque l'utilisateur clique sur une image de la frise le diaporama se met a jour pour afficher l'evenement correspondant
		 * @param e
		 */
	public void mouseClicked(MouseEvent e) {
		
		if (e.getClickCount() == 1) {
			
            JTable target = (JTable)e.getSource();
            
            int col = target.getSelectedColumn();
            int row = target.getSelectedRow();
            
            if(row >= 0 && row < 6 && col == 0) {
            	
            	gestionnaireCarte.first (panelCentre);
    			
    			indice = 0;
    			actuelIntitule.setText(chIntitulEtiquette[indice]);	
            	
            	
            }
            
            else if (row >= 0 && row < 6 && col == 32) {
            	
            	indice = 1;
    			actuelIntitule.setText(chIntitulEtiquette[indice]);
    			
    			gestionnaireCarte.show(panelCentre, chIntitulEtiquette[indice]);
            	
            }
            
            else if(row >= 0 && row < 6 && col == 68) {
            	
            	indice = 2;
    			actuelIntitule.setText(chIntitulEtiquette[indice]);
    			
    			gestionnaireCarte.show(panelCentre, chIntitulEtiquette[indice]);
            	
            }
            
            else if(row >= 0 && row < 6 && col == 76) {
            	
            	gestionnaireCarte.last (panelCentre);
    			
    			indice = 3;
    			actuelIntitule.setText(chIntitulEtiquette[indice]);
            	
            }

		}
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	};


	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
