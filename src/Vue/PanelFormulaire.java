package Vue;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.io.File;

import Modele.*;
import Controleur.*;

/**
 * 
 * @author touns
 *
 */
public class PanelFormulaire extends JPanel implements ActionListener{
	
	private JTextField chChemin = new JTextField(10);
	private JTextField chNom = new JTextField(10);
	private JTextField chTitre = new JTextField(10);											//On initialise tout les champs nécessaires
	private String [] tabMois = new String[13];
	private String [] tabJour = new String [32];
	private String [] tabAn = new String [89];
	private String [] tabPoids = new String[5];
	private JButton chBouton = new JButton ("Ajouter");
	//private JButton chBouton2 = new JButton ("<");
	//private JButton chBouton3 = new JButton (">");
	//private JButton chBouton4 = new JButton ("Rechercher");
	private JTextArea textDesc = new JTextArea (1, 5);
	JComboBox jour;
	JComboBox mois; 
	JComboBox annee; 
	JComboBox poids;
	
	ImageIcon img = new ImageIcon();
	Chronologie c;
	
	/**
	 * Constructeur
	 * @param parChrono
	 */
	public  PanelFormulaire(Chronologie parChrono){
		
	c = parChrono;
	
	Date d = new Date(13, 7, 1930);
		
	setLayout(new GridBagLayout());												// On utilise un GridBagLayout afin de mieux différenicer le diaporama et le formulaire
	GridBagConstraints contrainte = new GridBagConstraints ();
	contrainte.fill = GridBagConstraints.BOTH;
	contrainte.insets = new Insets(10, 10, 10, 10);
		
		for (int i = 1 ; i < tabJour.length ; i++) {
			tabJour[i] = Integer.toString(i);
		}
		
		for (int i = 1 ; i < tabMois.length ; i++) {
			tabMois[i] = Integer.toString(i);
		}
		
		for(int i = 0; i < tabAn.length; i++) {
			
			if(i%4 == 0) {
				
			tabAn[i] = " " + d.getAnnee() + " ";
			d = d.dateCoupeDuMonde();
			
			}
		}
		
		
		for (int i = 1 ; i <= 4 ; i++) {
			tabPoids[i] = Integer.toString(i);
		}
		
		jour = new JComboBox(tabJour);
		mois = new JComboBox(tabMois);
		annee = new JComboBox(tabAn);
		poids = new JComboBox(tabPoids);
		
		contrainte.gridx = 5;											//coordonée des champs du formulaire
		contrainte.gridy = 0;
		JLabel labChemImg = new JLabel("Image (Chemin)");
		add (labChemImg, contrainte);
		contrainte.gridx = 6;
		contrainte.gridwidth = 4;
		add (chChemin, contrainte);
		
		contrainte.gridx = 5;											//coordonée des champs du formulaire
		contrainte.gridy++;
		JLabel labNomImg = new JLabel("(Nom+format)");
		add (labNomImg, contrainte);
		contrainte.gridx = 6;
		contrainte.gridwidth = 4;
		add (chNom, contrainte);
		
		contrainte.gridx = 5;											//coordonée des champs du formulaire
		contrainte.gridy++;
		JLabel labTitre = new JLabel("Titre");
		add (labTitre, contrainte);
		contrainte.gridx = 6;
		contrainte.gridwidth = 4;
		add (chTitre, contrainte);
		
		contrainte.gridx = 5;
		contrainte.gridy++;
		JLabel labDate = new JLabel ("Date");
		add (labDate, contrainte);
		contrainte.gridx = 6;
		contrainte.gridwidth = 1;
		add (jour, contrainte);
		contrainte.gridx = 7;
		contrainte.gridwidth = 1;
		add (mois, contrainte);
		contrainte.gridx = 8;
		contrainte.gridwidth = 1;
		add (annee, contrainte);

		contrainte.gridx = 5;
		contrainte.gridy++;
		JLabel labPoids = new JLabel ("Poids");
		add (labPoids, contrainte);
		contrainte.gridx = 6;
		contrainte.gridwidth = 4;
		add (poids, contrainte);
		
		contrainte.gridx = 5;
		contrainte.gridy++;
		JLabel labDesc = new JLabel("Description");
		add (labDesc, contrainte); 
		contrainte.gridx = 7;
		add (textDesc, contrainte);
		
		
		contrainte.gridx = 5;
		contrainte.gridy++;
		contrainte.gridwidth = 4;
		add (chBouton, contrainte);
	
		chBouton.addActionListener(this);	
		
		
	}

	/**
	 * guetteur retournant le bouton
	 * @return JButton
	 */
	public JButton getBouton() {
		return chBouton;
	}
	

	/**
	 * guetteur retourant une date
	 * @return Date
	 */
	public Date getDate() {
		int j = jour.getSelectedIndex();
		int m = mois.getSelectedIndex();
		int a = annee.getSelectedIndex();
		
		Date date = new Date(j, m, a);
		return date;
	}

	/**
	 * guetteur retournat le chemin d'acces a l'image
	 * @return String
	 */
	public String getPathImg() {
		
		return chChemin.getText();
		
	}
	
	/**
	 * guetteur retournant le Nom et le format de l'iamge
	 * @return String
	 */
	public String getNomImg() {
		
		return chNom.getText();
		
	}
	
	/**
	 * guetteur retourant une ImageIcon
	 * @return ImageIcon
	 */
	public ImageIcon getImg() {
		
		return new ImageIcon(getPathImg()+File.separator+getNomImg());
		
	}
	

	/**
	 * Retourne l'evenement
	 * @return Evenement
	 */
	public  Evenement getEvt(){
		
		ImageIcon img = getImg();
		
		int jourD = jour.getSelectedIndex();
		int moisD= mois.getSelectedIndex();
		int anD = annee.getSelectedIndex();
		Date date = new Date(jourD, moisD, anD);
		
		String titre = chTitre.getText();
		int poidsE = poids.getSelectedIndex();
		String desc = textDesc.getText();
		
	
		Evenement e = new Evenement(img, date, titre, poidsE, desc);
		return e; 
	}

	/**
	 * Action gere par le Controleur
	 * @param parCtrl
	 */
	public void enrEcouteur(Controleur parCtrl) {
		
		chBouton.addActionListener(parCtrl);
		
	}
	
	/**
	 * Lord d'un clic l'evenement est ajoute a la chronologie
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == chBouton) {
			
			Chronologie ce = new Chronologie();
			
			Evenement evt = this.getEvt();
			ce.ajout(evt);
			System.out.println(ce.toString());
			
		}
	
	}


}