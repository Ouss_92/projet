package Vue;
import java.awt.*;
import java.io.File;

import javax.swing.*;

import Controleur.Controleur;
import Modele.*;

/**
 * 
 * @author touns
 *
 */
public class PanelChronologie extends JPanel {
	
	Chronologie c = new Chronologie();
	
	/**
	 * Constructeur
	 */
	public PanelChronologie (){
		setLayout(new GridLayout(1,3,10,10));	

		init_chr();
		
		c.toString();
		PanelFormulaire p = new PanelFormulaire(c);
		
		PanelFrise panelFrise = new PanelFrise(c, p);
		add(panelFrise);
		
		//Controleur control = new Controleur(parChrono, panelCalend);
		
	}
	
	/**
	 * Initialise un fichier
	 */
	public void init_chr(){
		File fichier = new File("saves"+File.separator+"chrono_2018.ser");
		
		if (fichier.length() != 0)
			c = (Chronologie) LectureEcriture.lecture(fichier);
	}
}