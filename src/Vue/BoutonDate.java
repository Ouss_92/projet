package Vue;

import java.util.*;
import java.awt.event.*;
import javax.swing.JButton;

import Controleur.Controleur;
import Modele.Date;

/**
 * 
 * @author touns
 *
 */
public class BoutonDate extends JButton {

	private Date dateJour;
	
	/**
	 * Constructeur
	 * @param parDate
	 */
	public BoutonDate(Date parDate) {
		super(Integer.toString(parDate.getJour()));
		dateJour = parDate;
	}
	
	/**
	 * 
	 * @param parBdate
	 */
	public BoutonDate(BoutonDate parBdate){
		dateJour = parBdate.dateJour;
	}
	
	/**
	 * guetteur retoruant la date
	 * @return
	 */
	public Date getDate() {
		return dateJour;
	}

	/**
	 * L'action sera gere par le controleur
	 * @param parCtrl
	 */
	public void enrEcouteur(Controleur parCtrl) {
		addActionListener(parCtrl);
	}


		
}