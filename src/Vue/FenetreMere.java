package Vue;

import javax.swing.*;


import Controleur.Controleur;
import Modele.Chronologie;

import java.awt.*;
import java.awt.event.*;

/**
 * 
 * @author touns
 *
 */
public class FenetreMere extends JFrame implements ActionListener{
	
	JMenuBar menubar = new JMenuBar();
	JMenu creation= new JMenu("Cr�ation");
	JMenu affichage = new JMenu("Affichage");
	JMenu quitter = new JMenu("Quitter");
	JMenu aPropos = new JMenu("?");
	
	JMenuItem creeEvt = new JMenuItem("Cr�er un nouvel �venement");
	JMenuItem afficher = new JMenuItem("Afficher � nouveau");
	JMenuItem quitter_app = new JMenuItem("Quitter l'application");
	JMenuItem aide = new JMenuItem("A propos ...");
	
	JButton chBouton = new JButton();
	
	Chronologie c;
	
	/**
	 * 
	 * @param parTitre
	 */
	public FenetreMere (String parTitre){
		
		super("Coupe du monde");
		
		menubar.add(creation);
		creation.add(creeEvt);
		creeEvt.addActionListener(this);
		
		menubar.add(affichage);
		affichage.add(afficher);
		affichage.addActionListener(this);
		
		menubar.add(quitter);
		quitter.add(quitter_app);
		quitter_app.addActionListener(this);
		
		menubar.add(aPropos);
		aPropos.add(aide);
		aPropos.addActionListener(this);
		
		setJMenuBar(menubar);
		
		
		PanelChronologie contentPane = new PanelChronologie();
		setContentPane(contentPane);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setExtendedState(this.MAXIMIZED_BOTH);
		setVisible(true);
		
	}
	
	/**
	 * Constructeur
	 * @param parTitre
	 * @param indice
	 */
	public FenetreMere(String parTitre, int indice) {
		
		super("Le formulaire");
		
		PanelFormulaire contentPane = new PanelFormulaire(c);
		setContentPane(contentPane);
		setSize(500,500);setLocation(500,50);
		setVisible(true);
		
	}
	
	/**
	 * 
	 */
	public Insets getInsets(){
		return new Insets(50,10,10,10);
	}
	
	/**
	 * Controleur gere l'action
	 * @param parCtrl
	 */
	public void enrEcouteur(Controleur parCtrl) {
		creeEvt.addActionListener(parCtrl);
		quitter_app.addActionListener(parCtrl);
	}

	/**
	 * actions liees aux clics de l'utilisateur dans l'onglet
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		
		
		
		if (e.getSource() == getCreeEvt()) {
			
			FenetreMere fen = new FenetreMere("Formulaire", 1);
			
		}
		
		else if(e.getSource() == getAffichage()) {
			
			
		}
		
		else if(e.getSource() == getQuitter()) {
			
			System.exit(0);
			
		}
		
		else if(e.getSource() == getAide()) {
			
			
		}
		
	}

	/**
	 * guetteur retourant l'item creer un evenement
	 * @return JMenuItem
	 */
	public JMenuItem getCreeEvt() {
		return creeEvt;
	}
	
	/**
	 * guetteur retourant l'tem afficher
	 * @return JMenuItem
	 */
	public JMenuItem getAffichage() {
		return afficher;
	}
	
	/**
	 * guetteur retourant l'tem quitter afin de quitter le programme
	 * @return JMenuItem
	 */
	public JMenuItem getQuitter() {
		return quitter_app;
	}
	
	/**
	 * guetteur retourant l'item aide
	 * @return JMenuItem
	 */
	public JMenuItem getAide() {
		return aide;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main (String []args){
		String str = "Coupe du monde";
		new FenetreMere(str);
	}	
	
	
}