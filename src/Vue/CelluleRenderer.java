package Vue;

import java.awt.Component;

import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.*;

/**
 * 
 * @author touns
 *
 */
public class CelluleRenderer extends JLabel implements TableCellRenderer {
	
	JLabel chLabel = new JLabel();
	int chRow = 0;
	int chCol = 0;
	
	ImageIcon chIcone[] = {new ImageIcon("images"+File.separator+"cdmu.png"), new ImageIcon("images"+File.separator+"cdmic.png"), new ImageIcon("images"+File.separator+"cdmf.png"), new ImageIcon("images"+File.separator+"cdmv.png")};
	
	//ImageIcon chIcone = new ImageIcon("Images"+File.separator+"cdmu.png");
	
	/**
	 * Constructeur
	 */
	public CelluleRenderer() {
		
		super();
		
		setOpaque(true);
		setHorizontalAlignment(JLabel.CENTER);
		this.setForeground(new java.awt.Color(180, 100, 40));
		
		/*for(int i = 0 ; i < chImage.length; i++) {
			
			  chImage[i] = i + path;
			
		}*/
		
	}

	/**
	 * Edite une cellule afin d'y introduire une image
	 * @return Component
	 */
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
		    
			//chLabel.setText((String) value);
		
			switch(column) {
			
				case 0:
					chLabel.setIcon(chIcone[0]);
				    return chLabel;
				
				case 32:
					chLabel.setIcon(chIcone[1]);
					return chLabel;
				   
				case 68:
					chLabel.setIcon(chIcone[2]);
				    return chLabel;
				    
				case 76:
					chLabel.setIcon(chIcone[3]);
					return chLabel;
				
				default:
					return chLabel;
			
			}
	
	}
	

}
